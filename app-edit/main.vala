int main(string[] args) {
	Gtk.init(ref args);

	(new Settings("sh.carbon.gde-panel")).bind("force-dark",
            Gtk.Settings.get_default(), "gtk_application_prefer_dark_theme",
            SettingsBindFlags.DEFAULT);

    var app = new DesktopAppInfo(args[1]);
    var new_filename = Environment.get_user_data_dir() + "/applications/" + app.get_id();

	var edit_dialog = new Gtk.Dialog.with_buttons("Edit", null, Gtk.DialogFlags.USE_HEADER_BAR);
	edit_dialog.add_button("_Save", Gtk.ResponseType.ACCEPT);
	var save_btn = edit_dialog.get_widget_for_response(Gtk.ResponseType.ACCEPT);
	save_btn.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
    save_btn.grab_default();
	edit_dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL);
	edit_dialog.destroy.connect(Gtk.main_quit);
	edit_dialog.width_request = 200;

	var content = edit_dialog.get_content_area();
	content.margin = 8;
	content.spacing = 12;

    var icon_def = app.get_string("Icon");
    var icon_edit_btn = new Gtk.Button();
    var icon_edit_img = new Gtk.Image.from_gicon(app.get_icon(), Gtk.IconSize.INVALID);
    icon_edit_img.pixel_size = 56;
    icon_edit_btn.add(icon_edit_img);
    icon_edit_btn.halign = Gtk.Align.CENTER;
    icon_edit_btn.clicked.connect(() => {
        var chooser = new Gtk.FileChooserNative(
            null, edit_dialog,
            Gtk.FileChooserAction.OPEN,
            null, null
        );
        chooser.set_current_folder("/usr/share/icons");

        var filter = new Gtk.FileFilter();
        filter.add_mime_type("image/*");
        chooser.filter = filter;

        if (chooser.run() == Gtk.ResponseType.ACCEPT)
            icon_edit_img.file = icon_def = chooser.get_filename();
    });
    content.add(icon_edit_btn);

    var name_entry = new Gtk.Entry();
    name_entry.text = app.get_name();
    name_entry.activates_default = true;
    content.add(name_entry);

    var desc_entry = new Gtk.Entry();
    desc_entry.placeholder_text = "Description";
    desc_entry.text = app.get_description();
    name_entry.activates_default = true;
    content.add(desc_entry);

    var reset_btn = new Gtk.Button.with_label("Reset");
    reset_btn.clicked.connect(() => {
        FileUtils.remove(new_filename);
        edit_dialog.destroy();
    });
    content.add(reset_btn);

    var advanced_expander = new Gtk.Expander.with_mnemonic("_Advanced");
    advanced_expander.resize_toplevel = true;
    content.add(advanced_expander);
    var advanced = new Gtk.Box(Gtk.Orientation.VERTICAL, content.spacing);
    advanced.margin_top = content.spacing;
    advanced_expander.add(advanced);

    var exec_entry = new Gtk.Entry();
    exec_entry.placeholder_text = "Command";
    exec_entry.text = app.get_string("Exec");
    exec_entry.primary_icon_name = "system-run-symbolic";
    exec_entry.primary_icon_tooltip_text = "Command";
    exec_entry.activates_default = true;
    advanced.add(exec_entry);

    var keyword_entry = new Gtk.Entry(); // TODO: GTK4 tagged entry
    keyword_entry.placeholder_text = "Keywords";
    keyword_entry.text = app.get_string("Keywords");
    keyword_entry.primary_icon_name = "system-search-symbolic";
    keyword_entry.primary_icon_tooltip_text = "Keywords";
    keyword_entry.activates_default = true;
    advanced.add(keyword_entry);

    var terminal = new Gtk.CheckButton.with_label("Open in terminal");
    terminal.active = app.get_boolean("Terminal");
    advanced.add(terminal);

    var search_only = new Gtk.CheckButton.with_label("Only show in search");
    search_only.active = app.get_boolean("X-SearchOnly");
    advanced.add(search_only);

    var hide = new Gtk.CheckButton.with_label("Hide app entirely");
    hide.active = app.get_boolean("Hidden");
    advanced.add(hide);

	edit_dialog.response.connect(result => {
	    if (result != Gtk.ResponseType.ACCEPT) return;

        var file = new KeyFile();
        file.load_from_file(app.filename, KeyFileFlags.NONE);

        set_string(file, "Desktop Entry", "Icon", icon_def);
        set_string(file, "Desktop Entry", "Name", name_entry.text);
        set_string(file, "Desktop Entry", "Comment", desc_entry.text);
		file.set_string("Desktop Entry", "Exec", exec_entry.text);
		set_string(file, "Desktop Entry", "Keywords", keyword_entry.text);
		file.set_boolean("Desktop Entry", "Terminal", terminal.active);
		file.set_boolean("Desktop Entry", "X-SearchOnly", search_only.active);
		file.set_boolean("Desktop Entry", "Hidden", hide.active);

		file.save_to_file(new_filename);

		if (hide.active) { // Show a warning
		    var msg = new Gtk.MessageDialog(null, Gtk.DialogFlags.MODAL,
	            Gtk.MessageType.WARNING, Gtk.ButtonsType.OK, null);
            msg.text = "To restore this app, delete this file:";
            msg.secondary_text = new_filename;
	        msg.run();
		}
	});

    content.show_all();
	edit_dialog.run();
	return 0;
}

// Locale handling
static void set_string(KeyFile file, string group, string key, string val) {
    file.set_string(group, key, val);

    var locale = file.get_locale_for_key(group, key);
    if (locale != null)
        file.set_locale_string(group, key, locale, val);
}
