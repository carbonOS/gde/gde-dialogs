public int main(string[] args) {
	Gtk.init(ref args);

	(new Settings("sh.carbon.gde-panel")).bind("force-dark",
            Gtk.Settings.get_default(), "gtk_application_prefer_dark_theme",
            SettingsBindFlags.DEFAULT);

	var app = new DesktopAppInfo(args[1] + ".desktop");

	var dialog = new Gtk.MessageDialog(
	    null, // TODO
	    Gtk.DialogFlags.MODAL,
	    Gtk.MessageType.OTHER,
	    Gtk.ButtonsType.NONE,
	    "%s is Not Responding",
        app.get_name()
	);
	dialog.secondary_text =
	    "You can wait to see if it recovers or force it to quit entirely.";
	dialog.add_button("_Wait", Gtk.ResponseType.REJECT);
	var quit_btn = dialog.add_button("Force _Quit", Gtk.ResponseType.ACCEPT);
	quit_btn.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION);
	dialog.destroy.connect(Gtk.main_quit);

    var box = (dialog.message_area as Gtk.Box);
    var icon = new Gtk.Image.from_gicon(app.get_icon(), Gtk.IconSize.DIALOG);
    icon.show_all();
    box.pack_start(icon);
    box.reorder_child(icon, 0);

    var status = 0;
	dialog.response.connect(result => {
	    if (result == Gtk.ResponseType.ACCEPT)
	        status = 1; // Tell wayfire to force quit
	    else
	        status = 0; // Tell wayfire to do nothing
	});

	dialog.run();
	return status;
}
