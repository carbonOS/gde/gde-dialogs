public enum DialogType {
    LOGOUT = 0,
    SHUTDOWN,
    RESTART,
    INVALID
}

[DBus (name = "org.gnome.SessionManager.EndSessionDialog")]
public class DialogService : Object {
    private GSM session_manager;
    private Dialogs.DialogWindow win;
    private PowerConfirmDialog? cdialog = null;
    private PowerSelectDialog? sdialog = null;

    public DialogService(GSM mgr) {
        session_manager = mgr;

        win = new Dialogs.DialogWindow();
        win.dismissable = true;
        win.on_dismiss.connect(() => win.get_child().destroy());
    }

    public signal void confirmed_logout();
    public signal void confirmed_reboot();
    public signal void confirmed_shutdown();
    public signal void canceled();
    public signal void closed();

    public async void open(uint type, uint timestamp, uint timeout, ObjectPath[] inhibitors) throws Error {
        if (type >= DialogType.INVALID)
            throw new DBusError.INVALID_ARGS("Invalid dialog type");

        // Handle selection dialog
        if (sdialog != null) {
            var anim_cb = win.on_dismiss.connect_after(() => open.callback());
            win.dismiss();
            yield; // Wait for it to fade out
            win.disconnect(anim_cb);
        }

        // Update the dialog if it is already open
        if (cdialog != null) {
            if (cdialog.type == type)
                cdialog.update(inhibitors);
            else
                throw new DBusError.FAILED("Type does not match open dialog");
            return;
        }

        // Create the dialog and set up the widgets
        cdialog = new PowerConfirmDialog(win, (DialogType) type);
        cdialog.update(inhibitors);
        cdialog.logout.connect(() => confirmed_logout());
        cdialog.reboot.connect(() => confirmed_reboot());
        cdialog.shutdown.connect(() => confirmed_shutdown());
        ulong on_dismiss = 0;
        ulong on_cancel = 0;
        on_dismiss = win.on_dismiss.connect(() => {
            cdialog = null;
            win.visible = false;
            closed();
            win.disconnect(on_dismiss);
        });
        on_cancel = win.on_cancel.connect(() => {
            canceled();
            win.disconnect(on_cancel);
        });
        win.show_all();
    }

    public void close() throws Error {
        win.cancel();
    }

    public void toggle_prompt_for_action() throws Error {
        if (win.visible)
            close();
        else
            prompt_for_action();
    }

    public void prompt_for_action() throws Error {
        if (win.get_child() != null) return;

        sdialog = new PowerSelectDialog(win);
        sdialog.logout.connect(() => {
            session_manager.logout.begin(0 /* always prompt */);
        });
        sdialog.reboot.connect(() => {
            session_manager.reboot.begin();
        });
        sdialog.shutdown.connect(() => {
            session_manager.shutdown.begin();
        });
        ulong on_dismiss = 0;
        ulong on_cancel = 0;
        on_dismiss = win.on_dismiss.connect(() => {
            sdialog = null;
            win.disconnect(on_dismiss);
        });
        on_cancel = win.on_cancel.connect(() => {
            win.visible = false;
            win.disconnect(on_cancel);
        });
        win.show_all();
    }
}

int main(string[] args) {
    Dialogs.init(ref args);

    // Get a proxy to the session manager
    GSM mgr;
    try {
        mgr = Bus.get_proxy_sync<GSM>(BusType.SESSION,
            "org.gnome.SessionManager", "/org/gnome/SessionManager");
    } catch (Error e) {
        error("Failed to proxy GSM: %s", e.message);
    }

    // Publish the service
    Bus.own_name(
        BusType.SESSION, "sh.carbon.gde.EndSessionDialog",
        BusNameOwnerFlags.NONE,
        con => {
            try {
                con.register_object("/", new DialogService(mgr));
            } catch {
                error("Failed to register object");
            }
        }, null, () => error("Failed to obtain bus name")
    );

    Gtk.main();
    return 0;
}
