[Flags]
public enum GSMInhibitorFlag {
    LOGOUT      = 1 << 0,
    SWITCH_USER = 1 << 1,
    SUSPEND     = 1 << 2,
    IDLE        = 1 << 3,
    AUTOMOUNT   = 1 << 4;
}

[DBus (name = "org.gnome.SessionManager")]
public interface GSM : DBusProxy {
    public abstract async void logout(uint32 mode) throws Error;
    public abstract async void reboot() throws Error;
    public abstract async void shutdown() throws Error;
}

[DBus (name = "org.gnome.SessionManager.Inhibitor")]
public interface GSMInhibitor : DBusProxy {
    public abstract string get_app_id();
    public abstract string get_reason();
    public abstract GSMInhibitorFlag get_flags();
}

[DBus (name = "org.gnome.SessionManager.EndSessionDialog")]
public interface EndSessionDialog : DBusProxy {
    public abstract async void prompt_for_action() throws Error;
}
