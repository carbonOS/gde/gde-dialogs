class PowerSelectDialog : Dialogs.DialogBox {
    public signal void logout();
    public signal void reboot();
    public signal void shutdown();

    public PowerSelectDialog(Dialogs.DialogWindow win) {
        base(win);
        // TODO: Position on side of screen for mobile devices
        var actions = this.add_action_row();

        var shutdown_btn = create_button("_Shutdown", "system-shutdown-symbolic");
        shutdown_btn.clicked.connect(() => shutdown());
        actions.add(shutdown_btn);

        var reboot_btn = create_button("_Reboot", "system-reboot-symbolic");
        reboot_btn.clicked.connect(() => reboot());
        actions.add(reboot_btn);

        if (Environment.get_variable("XDG_SESSION_CLASS") != "greeter") {
            var logout_btn = create_button("_Log Out", "system-log-out-symbolic");
            logout_btn.clicked.connect(() => logout());
            actions.add(logout_btn);
        }

        var cancel_btn = create_button("_Cancel", "edit-delete-symbolic");
        cancel_btn.clicked.connect(() => win.cancel());
        actions.add(cancel_btn);

        // Make the cancel button the default widget
        cancel_btn.can_default = true;
        win.set_default(cancel_btn);
        win.visible = false; // Hack to get the window to respect the setting
        win.visible = true;
    }

    private Gtk.Button create_button(string title, string icon) {
        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        root.margin = 12;
        var img = new Gtk.Image.from_icon_name(icon, Gtk.IconSize.INVALID);
        img.pixel_size = 24;
        root.add(img);
        var lbl = new Gtk.Label.with_mnemonic(title);
        root.add(lbl);
        var btn = new Gtk.Button();
        btn.get_style_context().add_class("flat");
        btn.can_default = false;
        btn.add(root);
        return btn;
    }
}

class PowerConfirmDialog : Dialogs.DialogBox {
    public new DialogType type;

    public signal void logout();
    public signal void reboot();
    public signal void shutdown();

    private Gtk.Revealer inhibitor_revealer;
    private Gtk.Box inhibitor_box;

    public PowerConfirmDialog(Dialogs.DialogWindow win, DialogType type) throws Error {
        base(win);
        this.type = type;
        this.spacing = 0;

        string title_text;
        string subtitle_text;
        string button_mnemonic;
        switch (type) {
            case DialogType.LOGOUT:
                title_text = "Log Out";
                subtitle_text = "Are you sure you want to log out? This will close all open applications.";
                button_mnemonic = "_Log Out";
                break;
            case DialogType.SHUTDOWN:
                title_text = "Shutdown";
                subtitle_text = "Are you sure you want to power off? This will close all open applications and turn off this device.";
                button_mnemonic = "_Shutdown";
                break;
            case DialogType.RESTART:
                title_text = "Reboot";
                subtitle_text = "Are you sure you want to reboot? This will close all open applications and restart this device.";
                button_mnemonic = "_Reboot";
                break;
            default:
                assert_not_reached();
        }

        var title = new Gtk.Label(title_text);
        title.get_style_context().add_class("title");
        title.margin_bottom = 12;
        this.add(title);
        var subtitle = new Gtk.Label(subtitle_text);
        subtitle.justify = Gtk.Justification.CENTER;
        subtitle.margin_start = 12;
        subtitle.margin_end = 12;
        subtitle.max_width_chars = 30;
        subtitle.wrap = true;
        subtitle.margin_bottom = 12;
        this.add(subtitle);

        var inhibitor_section = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        inhibitor_section.margin_bottom = 12;
        var message = new Gtk.Label("<b>Some applications are inhibiting this action</b>");
        message.use_markup = true;
        message.get_style_context().add_class("error");
        message.show_all();
        inhibitor_section.add(message);
        inhibitor_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
        inhibitor_box.margin = 6;
        inhibitor_section.add(inhibitor_box);
        inhibitor_revealer = new Gtk.Revealer();
        inhibitor_revealer.transition_type = Gtk.RevealerTransitionType.SLIDE_DOWN;
        inhibitor_revealer.add(inhibitor_section);
        this.add(inhibitor_revealer);

        var actions = this.add_action_row();
        var cancel_button = new Gtk.Button.with_mnemonic("_Cancel");
        cancel_button.get_style_context().add_class("flat");
        cancel_button.clicked.connect(win.cancel);
        cancel_button.can_default = true;
        win.set_default(cancel_button);
        actions.add(cancel_button);
        var ok_button = new Gtk.Button.with_mnemonic(button_mnemonic);
        //ok_button.get_style_context().add_class("destructive-action");
        ok_button.get_style_context().add_class("flat");
        ok_button.clicked.connect(() => {
            switch (type) {
                case DialogType.LOGOUT:
                    logout();
                    break;
                case DialogType.SHUTDOWN:
                    shutdown();
                    break;
                case DialogType.RESTART:
                    reboot();
                    break;
            }
            win.dismiss();
        });
        actions.add(ok_button);
    }

    public void update(ObjectPath[] inhibitors_paths) {
        // Get the inhibitors & filter out irrelevant types
        GSMInhibitor[] inhibitors = {};
        foreach (var opath in inhibitors_paths) {
            var inhibitor = Bus.get_proxy_sync<GSMInhibitor>(BusType.SESSION,
                "org.gnome.SessionManager", opath);
            var flags = inhibitor.get_flags();
            if (GSMInhibitorFlag.LOGOUT in inhibitor.get_flags())
                inhibitors += inhibitor;
        }

        // Clear out the old inhibitors
        inhibitor_revealer.reveal_child = (inhibitors.length > 0);
        foreach (var child in inhibitor_box.get_children())
            child.destroy();

        // Show the new ones
        foreach (var inhibitor in inhibitors) {
            var appid = inhibitor.get_app_id();
            if (!appid.has_suffix(".desktop")) appid += ".desktop";
            var app = new DesktopAppInfo(appid) ??
                      new DesktopAppInfo("sh.carbon.Unknown.desktop");

            var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
            var icon = new Gtk.Image.from_gicon(app.get_icon(),
                Gtk.IconSize.INVALID);
            icon.pixel_size = 36;
            root.pack_start(icon, false);
            var textroot = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            root.add(textroot);
            var appname = new Gtk.Label("<b>%s</b>".printf(app.get_name()));
            appname.use_markup = true;
            appname.halign = Gtk.Align.START;
            textroot.add(appname);
            var reason = new Gtk.Label(inhibitor.get_reason());
            reason.justify = Gtk.Justification.LEFT;
            reason.halign = Gtk.Align.START;
            textroot.add(reason);
            inhibitor_box.add(root);
        }

        inhibitor_box.show_all();
    }
}
