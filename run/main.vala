void on_activate(string input, bool term) {
    // Create the launch context
    var ctx = Gdk.Display.get_default().get_app_launch_context();
    ctx.launched.connect((info, platform_data) => {
        int pid = -1;
        platform_data.lookup("pid", "i", &pid);
        Gnome.start_systemd_scope.begin(info.get_id(), pid, null, null, null);
    });

    // Special "r" command that restarts the shell
    //
    if (input == "r")
        input = "UNITS=$(systemctl --user --legend=false list-unit-files gde-* | awk '{print $1}'); " +
                "systemctl --user reset-failed $UNITS; " +
                "systemctl --user restart $UNITS";

    // TODO: Use Uri.is_valid once we upgrade vala
    if (Uri.parse_scheme(input) != null) {
        print("Launching default handler for URI: %s\n", input);
        try {
            AppInfo.launch_default_for_uri(input, ctx);
        } catch {
            error("Couldn't open default handler");
        }
    } else {
        // Escape the command line and wrap w/ bash
        var cmdline = "bash -c %s".printf(Shell.quote(input));
        print("Running command: %s\n", cmdline);

        // Launch the command
        var flags = term ? AppInfoCreateFlags.NEEDS_TERMINAL :
            AppInfoCreateFlags.NONE;
        var info = AppInfo.create_from_commandline(cmdline, null, flags);
        try {
            info.launch(null, ctx);
        } catch {
            error("Couldn't launch the command");
        }
    }
}

int main(string[] args) {
    // Setup
    Dialogs.init(ref args);
    var win = new Dialogs.DialogWindow();
    win.dismissable = true;
    win.on_dismiss.connect(Gtk.main_quit);
    var dialog = new Dialogs.DialogBox(win);

    // Add a title
    var title = new Gtk.Label("Enter a Command");
    title.get_style_context().add_class("title");
    dialog.add(title);

    // Add the main widgets
    var command_entry = new Gtk.Entry();
    command_entry.width_chars = 30;
    dialog.add(command_entry);
    command_entry.grab_focus_without_selecting();
    var terminal_switch = new Gtk.CheckButton.with_mnemonic("Run in _Terminal");
    dialog.add(terminal_switch);

    // Handle enter key
    command_entry.activate.connect(() => {
        on_activate(command_entry.text, terminal_switch.active);
        win.dismiss();
    });

    // Add the buttons
    var actions = dialog.add_action_row();
    var cancel_button = new Gtk.Button.with_mnemonic("_Cancel");
    cancel_button.get_style_context().add_class("flat");
    cancel_button.clicked.connect(win.dismiss);
    actions.add(cancel_button);
    var run_button = new Gtk.Button.with_mnemonic("_Run");
    run_button.get_style_context().add_class("flat");
    run_button.clicked.connect(() => {
        on_activate(command_entry.text, terminal_switch.active);
        win.dismiss();
    });
    actions.add(run_button);

    // Show everything
    win.show_all();
    Gtk.main();
    return 0;
}

