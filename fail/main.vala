int main(string[] args) {
	Gtk.init(ref args);

    Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;
    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_data("""
        window { background: black; }
        image { color: @error_color; }
    """);
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    );

    var window = new Gtk.Window();
    window.destroy.connect(Gtk.main_quit);
    window.fullscreen();

    var wbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
    wbox.valign = Gtk.Align.CENTER;
    wbox.halign = Gtk.Align.CENTER;

    var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
    root.set_center_widget(wbox);
    window.add(root);

    var icon = new Gtk.Image.from_icon_name("dialog-warning-symbolic", Gtk.IconSize.INVALID);
    icon.get_style_context().add_class("warning");
    icon.pixel_size = 85;
    wbox.add(icon);

    var title = new Gtk.Label(null);
    var markup = "<span weight='heavy' size='xx-large'>%s</span>";
    title.set_markup(markup.printf("Something has gone wrong"));
    wbox.add(title);

    var subtitle = new Gtk.Label("We're sorry: A problem has occured and the system could not recover. Please, try logging in again.");
    subtitle.wrap = true;
    subtitle.max_width_chars = 45;
    subtitle.justify = Gtk.Justification.CENTER;
    wbox.add(subtitle);

    var bbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
    bbox.margin_top = 6;
    wbox.add(bbox);

    var logout = new Gtk.Button.with_mnemonic("_Try Again");
    logout.clicked.connect(() => window.destroy());
    bbox.pack_start(logout);

    var reboot = new Gtk.Button.with_mnemonic("_Reboot");
    reboot.clicked.connect(() => {
        try {
            Process.spawn_command_line_async("systemctl reboot");
        } catch {
            critical("Couldn't reboot!");
            window.destroy();
        }
    });
    reboot.get_style_context().add_class("destructive-action");
    bbox.pack_start(reboot);

	window.show_all();
	Gtk.main();
	return 0;
}
