# gde-dialogs

This provides the various dialogs and agents that are part of the Graphite
Desktop Environment. These include:

- Run: The classic "run" prompt used for starting commands
- App Edit: A small dialog that provides some basic app-icon edb  iting for the GDE desktop.
- Fail: Used in the greeter to notify the user about a failed login
- Session: The end-session or shutdown/reboot dialog
- Polkit: Authentication dialog to allow unprivilaged users to take privilaged actionss
- Geoclue: Geoclue agent; allows user to accept/deny location access to apps


## Wayfire config

To function properly as-intended, there is some config that should be done
for Wayfire:

```
[animate]
fade_enabled_for = [...] | type equals "overlay"
```

## TODO

- Geoclue: Save selection in permissions db
- Ensure Geoclue works on the altest version
- NM-Secrets: Network secrets entry for NetworkManager
- Unresponsive: A dialog that will show up whenever an app stops responding to pings
from the display server, allowing the user to terminate it (BLOCKED: Needs support from wayfire)
