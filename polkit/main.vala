class Agent : PolkitAgent.Listener {

    public override async bool initiate_authentication(string action_id, string message,
            string icon_name, Polkit.Details details, string cookie,
            GLib.List<Polkit.Identity> identities,
            GLib.Cancellable? cancellable) throws Polkit.Error {
        if (identities == null) {
            return false;
        }

        var dialog = new PromptWindow(message, cookie, identities, cancellable);
        dialog.on_dismiss.connect(() => initiate_authentication.callback());
        dialog.show_all();
        yield;
        dialog.destroy();

        if (dialog.was_canceled) {
            throw new Polkit.Error.CANCELLED ("Authentication dialog was dismissed by the user");
        }

        return true;
    }

}

int main(string[] args) {
    Dialogs.init(ref args);

    // Set up the session
    Polkit.Subject? subject = null;
    try {
        var pid = Posix.getpid();
        subject = new Polkit.UnixSession.for_process_sync(pid);
    } catch (Error e) {
        error("Unable to init session: %s", e.message);
    }

    // Set up the agent
    try {
        var agent = new Agent();
        agent.register(PolkitAgent.RegisterFlags.NONE, subject, "/");
    } catch (Error e) {
        error("Failed to register agent: %s", e.message);
    }

    Gtk.main();
    return 0;
}
