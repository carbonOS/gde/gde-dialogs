// Adapted from elementary:
// https://github.com/elementary/pantheon-agent-polkit/blob/master/src/PolkitDialog.vala

// Used for the user icon
[DBus (name = "org.freedesktop.Accounts.User")]
public interface ActUser : DBusProxy {
        public abstract string icon_file { public owned get; }
}

class PromptWindow : Dialogs.DialogWindow {
    public bool was_canceled = false;

    private string cookie;
    private PolkitAgent.Session? pk_session = null;
    private Polkit.Identity? pk_identity = null;

    private Gtk.Box avatar_box;
    private Gtk.Button auth_button;
    private Gtk.ComboBox idents_combo;
    private bool only_one_user = false;
    private Gtk.Entry password_entry;
    private Gtk.Label password_feedback;
    private Gtk.Revealer feedback_revealer;

    private ulong error_signal_id;
    private ulong request_signal_id;
    private ulong info_signal_id;
    private ulong complete_signal_id;

    public PromptWindow(string message, string cookie, List<Polkit.Identity> idents,
            Cancellable cancellable) {
        this.dismissable = true;
        this.on_cancel.connect(() => {
            was_canceled = true;
            if (pk_session != null)
                pk_session.cancel();
        });

        this.cookie = cookie;
        cancellable.cancelled.connect(this.cancel);

        var dialog = new Dialogs.DialogBox(this);
        dialog.width_request = 200;

        var title = new Gtk.Label("Authentication Required");
        title.get_style_context().add_class("title");
        dialog.add(title);

        var subtitle = new Gtk.Label(message);
        subtitle.use_markup = true;
        subtitle.wrap = true;
        subtitle.max_width_chars = 35;
        subtitle.justify = Gtk.Justification.CENTER;
        dialog.add(subtitle);

        idents_combo = new Gtk.ComboBox();
        idents_combo.changed.connect(on_ident_changed);
        dialog.add(idents_combo);
        Gtk.CellRenderer renderer = new Gtk.CellRendererPixbuf();
        idents_combo.pack_start(renderer, false);
        idents_combo.add_attribute(renderer, "icon-name", 0);
        renderer = new Gtk.CellRendererText();
        renderer.xpad = 6;
        idents_combo.pack_start(renderer, true);
        idents_combo.add_attribute(renderer, "text", 1);
        idents_combo.set_id_column(1);

        avatar_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
        avatar_box.margin_top = avatar_box.margin_bottom = 6;
        avatar_box.no_show_all = true;
        dialog.add(avatar_box);

        var entry_and_revealer = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        dialog.add(entry_and_revealer);

        password_entry = new Gtk.Entry();
        password_entry.width_chars = 35;
        password_entry.placeholder_text = "Enter password";
        password_entry.primary_icon_name = "dialog-password-symbolic";
        password_entry.input_purpose = Gtk.InputPurpose.PASSWORD;
        password_entry.caps_lock_warning = true;
        password_entry.visibility = false;
        entry_and_revealer.add(password_entry);
        password_entry.grab_focus_without_selecting();
        password_entry.activate.connect(on_activate);

        feedback_revealer = new Gtk.Revealer();
        feedback_revealer.transition_type = Gtk.RevealerTransitionType.SLIDE_DOWN;
        entry_and_revealer.add(feedback_revealer);

        password_feedback = new Gtk.Label(null);
        password_feedback.margin_top = dialog.spacing;
        password_feedback.get_style_context().add_class("error");
        password_feedback.wrap = true;
        password_feedback.max_width_chars = 35;
        feedback_revealer.add(password_feedback);

        var actions = dialog.add_action_row();
        var cancel_button = new Gtk.Button.with_mnemonic("_Cancel");
        cancel_button.get_style_context().add_class("flat");
        cancel_button.clicked.connect(this.cancel);
        actions.add(cancel_button);

        auth_button = new Gtk.Button.with_mnemonic("_Authenticate");
        auth_button.get_style_context().add_class("flat");
        auth_button.clicked.connect(on_activate);
        actions.add(auth_button);

        populate_idents(idents);
    }

    private new void set_sensitive(bool sensitive) {
        idents_combo.sensitive = sensitive && !only_one_user;
        password_entry.sensitive = sensitive;
        auth_button.sensitive = sensitive;
    }

    private void on_activate() {
        set_sensitive(false);
        feedback_revealer.reveal_child = false;

        if (pk_session == null) select_session();
        pk_session.response(password_entry.text);
    }

    private void select_session () {
        if (pk_session != null) deselect_session ();

        pk_session = new PolkitAgent.Session (pk_identity, cookie);
        error_signal_id = pk_session.show_error.connect(show_error);
        complete_signal_id = pk_session.completed.connect(on_pk_session_completed);
        request_signal_id = pk_session.request.connect(on_pk_request);
        info_signal_id = pk_session.show_info.connect(text => info(text));
        pk_session.initiate ();
    }

    private void deselect_session () {
        if (pk_session != null) {
            pk_session.disconnect(error_signal_id);
            pk_session.disconnect(complete_signal_id);
            pk_session.disconnect(request_signal_id);
            pk_session.disconnect(info_signal_id);
            pk_session = null;
        }
    }

    private void populate_avatar(Posix.Passwd user) {
        var avatar = new Hdy.Avatar(72, user.pw_name, false);
        avatar_box.add(avatar);

        var proxy = Bus.get_proxy_sync<ActUser>(BusType.SYSTEM,
            "org.freedesktop.Accounts", "/org/freedesktop/Accounts/User" +
            "%u".printf((uint32) user.pw_uid));
        avatar.set_image_load_func(() => new Gdk.Pixbuf.from_file(
            proxy.icon_file));

        avatar_box.add(new Gtk.Label(user.pw_gecos ?? user.pw_name));

        avatar_box.no_show_all = false;
        avatar_box.show_all();
    }

    private void populate_idents(List<Polkit.Identity> idents) {
        var model = new Gtk.ListStore(3, typeof(string), typeof(string),
            typeof(Polkit.Identity));
        Gtk.TreeIter iter;

        unowned Posix.Passwd? target_user = null;
        int length = 0;
        int active = 0;

        foreach (var ident in idents) {
            if (ident == null) continue;

            string name = ident.to_string();
            string icon = "avatar-default-symbolic";

            if (ident is Polkit.UnixUser) {
                unowned Posix.Passwd? pwd = Posix.getpwuid(
                    ((Polkit.UnixUser) ident).get_uid());
                if (pwd != null) {
                    if (target_user == null && length < 2)
                        target_user = pwd;
                    name = pwd.pw_gecos ?? pwd.pw_name;
                }
            } else if (ident is Polkit.UnixGroup) {
                unowned Posix.Group? gwd = Posix.getgrgid(
                    ((Polkit.UnixGroup) ident).get_gid());
                if (gwd != null)
                    name = "Group: %s".printf(gwd.gr_name);
                icon = "system-users-symbolic";
            }

            model.append(out iter);
            model.set(iter, 0, icon, 1, name, 2, ident);

            if (name == Environment.get_user_name())
                active = length;
            length++;
        }

        idents_combo.set_model(model);
        idents_combo.active = active; // Select curr. user if possible

        if (length < 2) { // Hide if appropriate
            only_one_user = true;
            idents_combo.sensitive = false;

            if (target_user != null) {
                // Hide the combo box
                idents_combo.visible = false;
                idents_combo.no_show_all = true;

                // Populate & show the rich user icon
                populate_avatar(target_user);
            }

        }
    }

    private void on_ident_changed() {
        Gtk.TreeIter iter;
        if (!idents_combo.get_active_iter(out iter)) {
            deselect_session();
            return;
        }

        var model = idents_combo.get_model();
        if (model == null) return;
        model.get(iter, 2, out pk_identity, -1);
        select_session();
    }

    private void show_error(string text) {
        password_feedback.label = text;
        feedback_revealer.reveal_child = true;
    }

    private void on_pk_request(string request, bool echo_on) {
        password_entry.visibility = echo_on;
    }

    private void on_pk_session_completed (bool authorized) {
        set_sensitive(true);
        if (!authorized) {
            if (!was_canceled) {
                show_error("Authentication failed. Please try again.");
                deselect_session();
                password_entry.text = "";
                password_entry.grab_focus();
                select_session();
            }
        } else {
            this.dismiss();
        }
    }
}
