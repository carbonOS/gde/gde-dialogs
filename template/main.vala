int main(string[] args) {
    Dialogs.init(ref args);

    var win = new Dialogs.DialogWindow();
    win.dismissable = true;
    win.on_cancel.connect(() => print("Cancel!\n"));
    win.on_dismiss.connect(Gtk.main_quit);
    var dialog = new Dialogs.DialogBox(win);
    dialog.width_request = 250;
    //win.add_dialog(dialog, Gtk.Align.CENTER, Gtk.Align.END,
    //    Gtk.RevealerTransitionType.SLIDE_UP, 0, 0);

    var title = new Gtk.Label("Hello world!");
    title.get_style_context().add_class("title");
    dialog.add(title);

    var subtitle = new Gtk.Label("This is a system dialog. Enjoy the view!");
    dialog.add(subtitle);

    var actions = dialog.add_action_row();
    var cancel_button = new Gtk.Button.with_mnemonic("_Cancel");
    cancel_button.get_style_context().add_class("flat");
    cancel_button.clicked.connect(() => {
        print("Canceled\n");
        win.cancel();
    });
    actions.add(cancel_button);

    var ok_button = new Gtk.Button.with_mnemonic("_OK");
    ok_button.get_style_context().add_class("flat");
    ok_button.can_default = true;
    win.set_default(ok_button);
    ok_button.clicked.connect(() => {
        print("OK Pressed\n");
        win.dismiss();
    });
    actions.add(ok_button);

    win.show_all();
    Gtk.main();
    return 0;
}
