class PromptWindow : Dialogs.DialogWindow {
    public bool granted = false;

    public PromptWindow(string app_name, Icon ico, GClue.AccuracyLevel accuracy) {
        this.dismissable = true;
        this.on_cancel.connect(() => {
           granted = false;
        });

        var dialog = new Dialogs.DialogBox(this);
        dialog.width_request = 250;

        var icon = new Gtk.Image.from_gicon(ico, Gtk.IconSize.DIALOG);
        icon.pixel_size = 66;
        dialog.add(icon);

        var title = new Gtk.Label("Allow Location Access?");
        title.get_style_context().add_class("title");
        dialog.add(title);

        unowned string accuracy_str;
        switch (accuracy) {
            case GClue.AccuracyLevel.COUNTRY:
                accuracy_str = "the current country";
                break;
            case GClue.AccuracyLevel.CITY:
                accuracy_str = "the nearest city or town";
                break;
            case GClue.AccuracyLevel.NEIGHBORHOOD:
                accuracy_str = "the nearest neighborhood";
                break;
            case GClue.AccuracyLevel.STREET:
                accuracy_str = "the nearest street";
                break;
            case GClue.AccuracyLevel.EXACT:
                accuracy_str = "your exact location";
                break;
            default:
                accuracy_str = "your location";
                break;
        }
        var message = "<b>%s</b> will be able to detect %s.\n".printf(
            app_name, accuracy_str) +
            "Permissions can be changed in the <b>Privacy</b> settings";
        var subtitle = new Gtk.Label(message);
        subtitle.use_markup = true;
        subtitle.wrap = true;
        subtitle.max_width_chars = 45;
        subtitle.justify = Gtk.Justification.CENTER;
        dialog.add(subtitle);

        var actions = dialog.add_action_row();
        var deny_button = new Gtk.Button.with_mnemonic("_Deny");
        deny_button.get_style_context().add_class("flat");
        deny_button.clicked.connect(this.cancel);
        actions.add(deny_button);
        var allow_button = new Gtk.Button.with_mnemonic("_Allow");
        allow_button.get_style_context().add_class("flat");
        allow_button.clicked.connect(() => {
            granted = true;
            this.dismiss();
        });
        actions.add(allow_button);
    }
}
