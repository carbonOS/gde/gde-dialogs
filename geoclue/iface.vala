// Adapted from elementary:
// https://github.com/elementary/pantheon-agent-geoclue2/blob/master/src/Interfaces.vala

[DBus (name = "org.freedesktop.GeoClue2.Manager")]
public interface GeoClue2Manager : Object {
    public abstract void add_agent(string id) throws GLib.Error;
    public abstract async ObjectPath get_client() throws GLib.Error;
}

[DBus (name = "org.freedesktop.GeoClue2.Agent")]
public interface GeoClue2Agent : Object {
    public abstract async void authorize_app(string desktop_id, GClue.AccuracyLevel req_accuracy_level, out bool authorized, out GClue.AccuracyLevel allowed_accuracy_level) throws GLib.Error;
    public abstract GClue.AccuracyLevel max_accuracy_level { get; }
}

[DBus (name = "org.freedesktop.GeoClue2.Client")]
public interface GeoClue2Client : GLib.Object {
    public abstract void start() throws GLib.Error;
    public abstract void stop() throws GLib.Error;
    public abstract string desktop_id { owned get; set; }
}


