[DBus (name = "org.freedesktop.GeoClue2.Agent")]
class Agent : Object {
    private GeoClue2Manager mgr;
    private Settings settings;

    public GClue.AccuracyLevel max_accuracy_level {
        get {
            if (settings.get_value("enabled").get_boolean()) {
                return GClue.AccuracyLevel.EXACT;
            } else {
                return GClue.AccuracyLevel.NONE;
            }
        }
    }

    construct {
        settings = new Settings("org.gnome.system.location");
        settings.changed["enabled"].connect(() => {
            notify_property("max-accuracy-level");
        });
    }

    public async void authorize_app(string id, GClue.AccuracyLevel req_accuracy,
            out bool authorized, out GClue.AccuracyLevel allowed_accuracy) {

        allowed_accuracy = req_accuracy;
        if (mgr == null) {
            authorized = false;
            error("authorize_app called with no manager!");
            return;
        }

        var app = new DesktopAppInfo(id + ".desktop");
        if (app == null) {
            warning("Invalid app: %s. Rejecting.", id);
            authorized = false;
            return;
        }

        // TODO: Check remembered apps

        var client_path = yield mgr.get_client();
        var client = yield Bus.get_proxy<GeoClue2Client>(BusType.SYSTEM,
            "org.freedesktop.GeoClue2", client_path);
        if (client == null) {
            error("Coudln't obtain geoclue client!");
        }
        client.desktop_id = "sh.carbon.gde.GeoclueAgent";
        client.start();

        var dialog = new PromptWindow(app.get_name(), app.get_icon(),
            req_accuracy);
        dialog.on_dismiss.connect_after(() => authorize_app.callback());
        dialog.show_all();
        yield;
        dialog.destroy();

        client.stop();

        authorized = dialog.granted;

        // TODO: Remember app
    }

    [DBus(visible = false)]
    public void on_geoclue_appeared(DBusConnection con, string name, string owner) {
        mgr = con.get_proxy_sync(name, "/org/freedesktop/GeoClue2/Manager");
        try {
            mgr.add_agent("gnome-shell"); // We're GNOME, we swear!
            print("Ready.\n");
        } catch (Error e) {
            error("Couldn't register as an agent: %s", e.message);
        }
    }

    [DBus(visible = false)]
    public void on_geoclue_vanished(DBusConnection con, string name) {
        mgr = null;
    }
}



int main(string[] args) {
    Dialogs.init(ref args);

    var agent = new Agent();

    // Publish the service
    Bus.own_name(
        BusType.SESSION, "sh.carbon.gde.GeoclueAgent",
        BusNameOwnerFlags.NONE,
        con => {
            try {
                con.register_object("/org/freedesktop/GeoClue2/Agent", agent);

                // Once we have the name, look for geoclue to show up
                Bus.watch_name(BusType.SYSTEM, "org.freedesktop.GeoClue2",
                    BusNameWatcherFlags.AUTO_START, agent.on_geoclue_appeared,
                    agent.on_geoclue_vanished);
            } catch {
                error("Failed to register agent");
            }
        }, null, () => error("Failed to obtain bus name")
    );

    Gtk.main();
    return 0;
}
