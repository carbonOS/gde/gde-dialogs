namespace Dialogs {

    void init(ref unowned string[]? argv) {
        Gtk.init(ref argv);

        // Set up the dark mode
        var gde_settings = new Settings("sh.carbon.gde-panel");
        var gtk_settings = Gtk.Settings.get_default();
        gde_settings.bind("force-dark", gtk_settings,
            "gtk_application_prefer_dark_theme",
            SettingsBindFlags.DEFAULT);

        // Load the CSS
        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource("/sh/carbon/gde-dialogs/styles.css");
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );

        // Force adw-gtk3 theme
        gtk_settings.gtk_theme_name = "adw-gtk3";
    }

    class DialogWindow : Gtk.Window {
        public bool dismissable { set; get; default = false; }

        private Gtk.EventControllerKey esc_listener;
        private Gtk.Revealer? revealer = null;

        construct {
            GtkLayerShell.init_for_window(this);
            GtkLayerShell.set_layer(this, GtkLayerShell.Layer.OVERLAY);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.LEFT, true);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, true);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, true);
            GtkLayerShell.set_exclusive_zone(this, -1);
            GtkLayerShell.set_keyboard_interactivity(this, true);

            esc_listener = new Gtk.EventControllerKey(this);
            esc_listener.key_released.connect((val, code, state) => {
                if (dismissable && val == Gdk.Key.Escape)
                    cancel();
            });
        }

        public DialogWindow(bool blur = true) {
            if (blur) this.get_style_context().add_class("blur");
        }

        public void add_dialog(DialogBox dialog,
                Gtk.Align halign = Gtk.Align.CENTER,
                Gtk.Align valign = Gtk.Align.CENTER,
                Gtk.RevealerTransitionType transition =
                    Gtk.RevealerTransitionType.CROSSFADE,
                int horiz_adjustment = 0, int vert_adjustment = 0) {

            // Event box to suppress click events AND position the dialog
            var evt_box = new Gtk.EventBox();
            evt_box.halign = halign;
            evt_box.valign = valign;
            if (horiz_adjustment > 0)
                evt_box.margin_left = horiz_adjustment;
            else
                evt_box.margin_right = -horiz_adjustment;
            if (vert_adjustment > 0)
                evt_box.margin_top = vert_adjustment;
            else
                evt_box.margin_bottom = -vert_adjustment;

            // Revealer for animation
            revealer = new Gtk.Revealer();
            revealer.transition_type = transition;
            revealer.reveal_child = false;
            evt_box.bind_property("visible", revealer, "reveal-child",
                BindingFlags.SYNC_CREATE);

            // Add everything to the window
            revealer.add(dialog);
            evt_box.add(revealer);
            this.add(evt_box);
        }

        public override bool button_release_event(Gdk.EventButton evt) {
            if (!dismissable) return Gdk.EVENT_PROPAGATE;

            void* data;
            evt.any.window.get_user_data(out data);
            if (data == this) // Don't listen for clicks on child widgets
                cancel();
            return Gdk.EVENT_PROPAGATE;
        }

        public void dismiss() {
            _dismiss.begin(false);
        }

        public void cancel() {
            _dismiss.begin(true);
        }

        public signal void on_dismiss();
        public signal void on_cancel();

        private async void _dismiss(bool cancelled) {
            if (revealer != null) {
                revealer.notify["child-revealed"].connect(() =>
                    _dismiss.callback());
                revealer.reveal_child = false;
                yield; // Wait for the revealer
            }

            if (cancelled) on_cancel();
            on_dismiss();
        }
    }

    class DialogBox : Gtk.Box {
        construct {
            var ctx = this.get_style_context();
            ctx.add_class("background");
            ctx.add_class("dialogbox");

            this.orientation = Gtk.Orientation.VERTICAL;
            this.spacing = 12;
        }

        public DialogBox(DialogWindow? win = null) {
            if (win != null) win.add_dialog(this);
        }

        public Gtk.Box add_action_row() {
            var btn_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            btn_box.homogeneous = true;

            var ctx = btn_box.get_style_context();
            ctx.add_class("linked");
            ctx.add_class("actionrow");

            this.pack_end(btn_box, false);
            return btn_box;
        }
    }
}
